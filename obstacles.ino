
#include <avr/wdt.h>
#include "DeviceDriverSet_xxx0.h"

DeviceDriverSet_ULTRASONIC myUltrasonic;

void setup()
{
  Serial.begin(9600);
  myUltrasonic.DeviceDriverSet_ULTRASONIC_Init();
}

void loop()
{
  myUltrasonic.DeviceDriverSet_ULTRASONIC_Test();
}
